# contact-list

A single page app displaying a list of contacts

## How to run the project

Just use 'npm start'

## Technologies used

React.js
styled-components
axios
react-icons
prettier