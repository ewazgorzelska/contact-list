import styled from "styled-components";
import { FiSearch } from "react-icons/fi";

const SearchContainer = styled.div`
  display: flex;
  position: relative;
  border-bottom: 1px solid lightgrey;
  border-left: 1px solid darkgrey;
  border-right: 1px solid darkgrey;
`;

const SearchIcon = styled.div`
  position: absolute;
  z-index: 2;
  color: darkgrey;
  display: flex;
  align-items: center;
  justify-content: center;

  height: 100%;
  width: 5vw;
`;

const SearchInput = styled.input`
  width: 80vw;
  height: 5vh;
  padding-left: 4em;
  border: none;
`;

const SearchBar = () => {
  return (
    <SearchContainer>
      <SearchInput type="text" placeholder="Find a user" />
      <SearchIcon>
        <FiSearch />
      </SearchIcon>
    </SearchContainer>
  );
};

export default SearchBar;
