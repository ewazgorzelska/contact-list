import styled from "styled-components";

export const ListItem = styled.li`
  list-style: none;
  border-bottom: 1px solid darkgrey;

  display: flex;
  align-items: center;
  justify-content: space-around;
`;

export const UsersDataContainer = styled.div`
  display: flex;
  align-items: center;
  width: 60vw;
  padding: 1em;
  font-size: 1em;
  font-weight: 800;
`;

export const Avatar = styled.div`
  background-image: url(${({ avatar }) => avatar});
  background-position: center;
  background-repeat: no-repeat;
  width: 5vw;
  height: 10vh;
  border-radius: 50%;
`;

export const UsersFirstName = styled.div`
  margin-left: 3vw;
`;

export const UsersLastName = styled.div`
  margin-left: 1vw;
`;

export const Checkbox = styled.input`
  margin-left: 3vw;
  width: 5vw;
  height: 5vh;
`;
