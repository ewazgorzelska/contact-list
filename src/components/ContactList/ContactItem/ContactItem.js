import {
  ListItem,
  UsersDataContainer,
  Avatar,
  UsersFirstName,
  UsersLastName,
  Checkbox,
} from "./ContactItem.styles";

const ContactItem = ({ id, first_name, last_name, avatar, handleClick }) => {

  return (
    <ListItem id={id} onClick={(e) => handleClick(e)}>
      <UsersDataContainer>
        <Avatar avatar={avatar} />
        <UsersFirstName>{first_name}</UsersFirstName>
        <UsersLastName>{last_name}</UsersLastName>
      </UsersDataContainer>
      <Checkbox type="checkbox" />
    </ListItem>
  );
};

export default ContactItem;
