import { useState, useEffect } from 'react';
import styled from "styled-components";
import ContactItem from "./ContactItem/ContactItem";

const List = styled.div`
  background-color: #f6f6f6;
  border-left: 1px solid darkgrey;
  border-right: 1px solid darkgrey;
  border-bottom: 1px solid darkgrey;
`;

const ContactList = ({ users, isLoading, isError }) => {

  const [inputsChecked, setInputsChecked ] = useState([]);

  const handleClick = (e) => {
    setInputsChecked([...inputsChecked, e.currentTarget.id]);
  };

  useEffect(() => {
    console.log(inputsChecked);
  }, [inputsChecked]);

  return (
    <>
      {isError && <p>Something went wrong</p>}
      {isLoading ? (
        <p>Loading...</p>
      ) : (
        <List>
          {users
          .sort((a, b) => a.last_name.localeCompare(b.last_name, 'en'))
          .map(({ id, first_name, last_name, avatar }) => (
            <ContactItem
              key={id}
              id={id}
              first_name={first_name}
              last_name={last_name}
              avatar={avatar}
              handleClick={handleClick}
            />
          ))}
        </List>
      )}
    </>
  );
};

export default ContactList;
