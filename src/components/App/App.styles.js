import styled from "styled-components";

export const AppContainer = styled.div`
  width: 80vw;
  height: 90vh;
  overflow-y: scroll;

  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  display: flex;
  flex-direction: column;
`;
