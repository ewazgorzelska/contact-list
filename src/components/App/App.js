import { useState, useEffect } from "react";
import axios from "axios";
import TitleBar from "components/TitleBar/TitleBar";
import SearchBar from "components/SearchBar/SearchBar";
import ContactList from "components/ContactList/ContactList";
import { AppContainer } from "./App.styles";

const App = () => {
  const [users, setUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    axios
      .get(
        "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
      )
      .then((response) => {
        setUsers(response.data);
      })
      .catch(() => {
        setIsError(true)
      });
    setIsLoading(false);
  }, []);

  return (
    <AppContainer>
      <TitleBar />
      <SearchBar />
      <ContactList users={users} isLoading={isLoading} isError={isError} />
    </AppContainer>
  );
};

export default App;
